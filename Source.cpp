#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <list>



// ENUMERABLES

std::string Species[] = {
	"ahven",
	"hauki",
	"kiiski",
	"kuha",
	"lahna",
};
const int SpeciesCount = sizeof(Species) / sizeof(Species[0]);
int WeightRange[SpeciesCount][2] = {
	{ 50, 350 },
	{ 410, 6400 },
	{ 10, 300 },
	{ 400, 2500 },
	{ 100, 6000 }
};



// STRUCTS

struct Fish {
	std::string species = "none";
	int grams = 0;

	Fish() { }
	Fish(int level) {
		int seed = rand();
		species = Species[seed % SpeciesCount];
		grams = seed % (WeightRange[seed % SpeciesCount][1] - WeightRange[seed % SpeciesCount][0]) + WeightRange[seed % SpeciesCount][0];
	}
	bool is_empty() {
		if (species == "none") {
			return true;
		}
		else {
			return false;
		}
	}
};
struct Inventory {
	Fish fishes[256];
	int fishing_rod_level = 0;

	int fish_count() {
		int count = 0;
		while (fishes[count].is_empty() == false) {
			count++;
		}
		return count;
	}
};



// HELPER FUNCTIONS:

void print(int num) {
	std::cout << "  " << num << std::endl;
}
void print(std::string text) {
	std::cout << "  " << text << std::endl;
}



// MAIN FUNCITON

int main() {
	srand(time(NULL));
	Inventory inventory = Inventory();
	while (true) {
		std::string input = "";
		std::cin >> input;

		if (input == "kalasta") {
			Fish fish = Fish(0);
			print("Saalis: " + fish.species + " " + std::to_string(fish.grams) + "g.");
			inventory.fishes[inventory.fish_count()] = fish;
		}
		else if (input == "inventori") {
			print("Inventorisi sisältää:");
			if (inventory.fish_count() == 0) {
				print("Ei mitään.");
			}
			for (int i = 0; i < inventory.fish_count(); i++) {
				print(inventory.fishes[i].species + "\t" + std::to_string(inventory.fishes[i].grams));
			}
		}
		else if (input == "help" || input == "apua" || input == "komennot") {
			print("lopeta, sulje \t Sulje ohjelma.");
			print("kalasta \t\t Koeta onneasi.");
			print("inventori \t\t Tarkastele inventoriasi.");
		}
		else if (input == "lopeta" || input == "sulje") {
			break;
		}
		else {
			print("Tuntematon komento.");
		}
	}
}